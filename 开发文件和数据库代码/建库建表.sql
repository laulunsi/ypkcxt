CREATE database 药品工作站管理系统
on
(name='药品工作站管理_DATA',
 filename='c:\药品工作站管理系统_DATA.MDF',
 size=10,
 maxsize=50,
 filegrowth=2)
log on
( name='药品工作站管理_LOG',
  filename='c:\药品工作站管理系统_LOG.LDF',
  size=2,
  maxsize=10,
  filegrowth=1)

go

CREATE TABLE 药品信息
(
药品编号 INT NOT NULL PRIMARY KEY,
药品名称 CHAR(40) NOT NULL,
药品分类 CHAR(20) NOT NULL,
药品规格 CHAR(20),
包装规格 CHAR(20),
制造商编号 INT NOT NULL,
有效期 CHAR(20),
进药单价 MONEY CHECK(进药单价>0),
卖药单价 MONEY CHECK(卖药单价>0),
库存 INT NOT NULL,
)

CREATE TABLE 药品库存信息
(
药品编号 INT NOT NULL,
药品名称 CHAR(40) NOT NULL,
入库数量 INT CHECK(入库数量>0),
有效期至 CHAR(40),
)

CREATE TABLE 操作员信息
(
操作员编号 INT NOT NULL PRIMARY KEY,
姓名 CHAR(20) NOT NULL,
性别 bit NOT NULL,
联系电话 CHAR(40) NOT NULL
)
CREATE TABLE 采购计划
(
采购编号 INT NOT NULL PRIMARY KEY,
制造商编号 INT NOT NULL,
药品名称 CHAR(40) NOT NULL,
药品数量 INT NOT NULL,
药品单位 CHAR(20),
进药单价 MONEY CHECK(进药单价>0),
卖药单价 MONEY CHECK(卖药单价>0)
)

CREATE TABLE 药品入库表
(
入库编号 CHAR(40) NOT NULL PRIMARY KEY,
药品编号 INT NOT NULL,
入库数量 INT CHECK(入库数量>0),
入库时间 DATETIME NOT NULL,
有效期至 CHAR(40),
操作员 CHAR(40) NOT NULL,
)
CREATE TABLE 药品出库表
(
出库编号 CHAR(40) NOT NULL PRIMARY KEY,
药品编号 INT NOT NULL,
药品名称 char(20) NOT NULL,
出库数量 INT NOT NULL,
出库时间 DATETIME NOT NULL,
操作员 CHAR(40) NOT NULL,
)

CREATE TABLE 药品调价表
(
流水编号 INT NOT NULL PRIMARY KEY,
药品编号 INT NOT NULL,
调后售价 MONEY CHECK(调后售价>0),
调价时间 DATETIME NOT NULL,
操作员 CHAR(40) NOT NULL,
)
CREATE TABLE 管理员信息
(
用户名 VARCHAR(40) NOT NULL PRIMARY KEY,
密码 VARCHAR(40) NOT NULL
)
CREATE TABLE 包装单位
(
编号 INT NOT NULL PRIMARY KEY,
单位 CHAR(20) NOT NULL,
)


CREATE TABLE 制造商信息
(
编号 INT NOT NULL PRIMARY KEY,
名称 VARCHAR(40) NOT NULL,
省份编号 INT NOT NULL,
详细地址 VARCHAR(40) NOT NULL,
联系电话 VARCHAR(20)NOT NULL,
)

CREATE TABLE 国家
(
国家编号 INT NOT NULL PRIMARY KEY,
名称 CHAR(20) NOT NULL,
)

CREATE TABLE 省份
(
省份编号 INT NOT NULL PRIMARY KEY,
名称 CHAR(20) NOT NULL,
国家编号 INT  NOT NULL,
)
